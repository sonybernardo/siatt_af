# Fraud Detection

### Description <br>
This is a Setup of the Frontend for a Fraud Detection platform, this is Built in Pure/Vanilla JavaScript as well as is using Vis.js and Noe4j as nuclear libraries.

This Frontend renders Graphs to allow indentify connections between different entities to support fraud investigation, for this reason, there are some very important JavaScript implementation which allow some Dynamic as well as code execution based on a Decision tree.

The backend of this application can be found on [this repository](https://gitlab.com/sonybernardo/siat_af_integrador).

Note: Since this is a setup (POC), this was and can be extended/used as fundantions for Bigger project which Fraud Detection is the main purpose
