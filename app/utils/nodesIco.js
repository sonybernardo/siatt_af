nodesIco = {
    usergroups: {
        shape: "icon",
        icon: {
        face: "'FontAwesome'",
        code: "\uf0c0",
        size: 50,
        color: "#57169a",
        },
    },
    Person: {
        shape: "icon",
        icon: {
        face: "'FontAwesome'",
        code: "\uf007",
        size: 50,
        color: "#aa00ff",
        },
    },
    Customer: {
        shape: "icon",
        icon: {
        face: "'FontAwesome'",
        code: "\uf007",
        size: 50,
        color: "#aa00ff",
        },
    },
    Order: {
        shape: "icon",
        icon: {
        face: "'FontAwesome'",
        code: "\uf15c",
        size: 50,
        color: "orange",
        },
    },
    Payment: {
        shape: "icon",
        icon: {
        face: "'FontAwesome'",
        code: "\uf155",
        size: 30,
        color: "#0d70abdb",
        },
    },
    Employee: {
        shape: "icon",
        icon: {
        face: "'FontAwesome'",
        code: "\uf2b5",
        size: 30,
        color: "darkgreen",
        },
    }
}