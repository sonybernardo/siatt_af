
request = {
    viewController: new RequestObject(),
}


function RequestObject(){

    this.url = null;

    this.get = function({form, callBack, url, showModal}){

        let xhr = new XMLHttpRequest();

        if(showModal != false)
            document.getElementById("spinnerContainer").style.display = "flex";

        xhr.open("GET",url || this.url,true);
        xhr.send(form || "");

        xhr.onreadystatechange = function(){

            if(xhr.readyState == 4){
                if(xhr.status){
                    console.log(xhr.status);
                    if(callBack)
                        callBack(xhr.responseText);
                }
                document.getElementById("spinnerContainer").style.display = "none";
            }
        }
    }

    this.post = function({form, callBack, url, showModal, json}){

        let xhr = new XMLHttpRequest();

        if(showModal != false)
            document.getElementById("spinnerContainer").style.display = "flex";

        xhr.open("POST",url || this.url,true);

        if(json)
            xhr.setRequestHeader("Content-type","application/json");

        xhr.send(form || "");

        xhr.onreadystatechange = function(){

            if(xhr.readyState == 4){
                if(xhr.status){
                    console.log(xhr.status);
                    if(callBack)
                        callBack(xhr.responseText);
                }
                document.getElementById("spinnerContainer").style.display = "none";
            }
        }
    }

    return this;


}