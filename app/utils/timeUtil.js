function aoDate(dateString){

    let _date = new Date(dateString.toString());
    return `${_date.getDate()}-${_date.getMonth()}-${_date.getFullYear()}`;

}


function countSeconds(conteiner){

    let container = document.getElementById(conteiner);
    let seconds = 0;

    return setInterval(() => {
        container.innerHTML = "("+(++seconds)+"s)";
    },1000);

}

function stopCountSeconds(counter){
    clearInterval(counter);
}

timeUtil = {
    countSeconds : countSeconds,
    stopCountSeconds : stopCountSeconds,
    aoDate: aoDate
}