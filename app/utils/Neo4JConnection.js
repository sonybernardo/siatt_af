__neo4j__ = {
    connection: new Neo4JConnection(),
}


function Neo4JConnection(){

    this.session = null;

    this.init = function(){

        let driver = neo4j.driver(
            'neo4j://localhost',
            neo4j.auth.basic('neo4j', '1234')
        );
      
        this.session = driver.session();
        //this.session = session;
        return this.session;
    }


    this.fetchData = function(query, callBack){

        this.init();

        let readTxResultPromise = this.session.readTransaction(txc => {

            let queryString = query || 'MATCH (p:Customer) RETURN *';

            return txc.run(queryString);
        });

        readTxResultPromise
        .then(res => callBack(res))
        .catch(err => {
            console.log("Erro na query: ",err);
            alert("Erro ao executar query: ",err);
        });

    }

    this.writeData = function(query, callBack){

        this.init();

        let writeTxResultPromise = this.session.writeTransaction(txc => {
            let queryString = query;
            return txc.run(queryString);
        });

        writeTxResultPromise
        .then(res => callBack(res))
        .catch(err => {
            console.log("Erro na query: ",err);
            alert("Erro ao executar query: ",err);
        });

    }


    return this;


}