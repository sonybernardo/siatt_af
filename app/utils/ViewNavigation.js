view = {
    navigation: new ViewNavigation(),
    views : {
        'dashaboard' : 'pages/dashboard/index.html',
        'cases' : 'pages/casos/index.html',
        'cases-details' : 'pages/casos/detalhes.html',
        'new-case' : 'pages/analise/nova.html',
        'workflow' : 'pages/workflow/index.html',
        'rule' : 'pages/configuration/rule.html',
        'analyses-config' : 'pages/configuration/analyse.html',
    },
    activeView: null,
    activeConfigView: null,
    processView: null,
}


function ViewNavigation(){

    this.views = {};

    this.init = function(){
        this.views = {...view.views};
    }

    this.callView = function(viewName){

        this.init();
        this.parseView(viewName);
        view.activeView = viewName;
    }

    this.callViewWithHandler = function(viewToShow,handler,params){
        
        this.init();    
        this.parseViewWithHandler(viewToShow,handler,params);
        view.activeView = viewToShow;

    }

    this.callConfigWithHandler = function(viewToShow,handler,params){
        
        this.init();    
        this.parseConfigViewWithHandler(viewToShow,handler,params);
        view.activeConfigView = viewToShow;

    }

    this.parseView = function(viewToShow){

        requestObject = {
            callBack: function(dados){
                document.getElementById('app-main__inner').innerHTML = dados;      
            },
            url: this.views[viewToShow],
        };

        request.viewController.get(requestObject);

    }

    this.parseViewWithHandler = function(viewToShow, handler, params){

        requestObject = {
            callBack: function(dados){

                document.getElementById('app-main__inner').innerHTML = dados;
                if(handler != undefined)
                    handler(params || null);

            },
            url: this.views[viewToShow],
        };

        request.viewController.get(requestObject);        

    }

    this.parseConfigViewWithHandler = function(viewToShow, handler, params){

        if(viewToShow != view.activeConfigView){

            requestObject = {
                callBack: function(dados){

                    document.getElementById('configView').innerHTML = dados;
                    if(handler != undefined)
                        handler(params || null);

                },
                url: this.views[viewToShow],
            };

            request.viewController.get(requestObject);

        }

                

    }

    return this;

}