cases = {
    viewController: new CasesViewController(),
}

listStatus = {"show": "block", "hide": "none"}

function CasesViewController() {

    this.init = function(){
        cases.viewController.getCases();

    }

    this.getCases = function(){

        let queryString = `MATCH p=(:Customer)-[r]->(:Payment) RETURN p LIMIT 20`;
        __neo4j__.connection.fetchData(queryString, cases.viewController.parseCases )
    }
    

    this.scoreByEntity = function(entity){

        let allScores = cases.viewController.calcScore(entity);
        let totalScore = 0;

        for(let i in allScores){
            totalScore += allScores[i];
        }

        return {
            total: totalScore,
            byfield: allScores,
        };
    }

    this.calcScore = function(conteudo){
        if(!rule.allRule.hasOwnProperty(conteudo.entity))
            return [];
        
        let ruleToApply = rule.allRule[conteudo.entity];
        let filedsToApply = Object.keys(conteudo);

        let curEntity = {};

        for(let f of filedsToApply){
            if(ruleToApply.hasOwnProperty(f)){

                let curRule = ruleToApply[f];
                let curField = 0;

                for(let rule of curRule){

                    let fiedlValue = isNaN(conteudo[f]) ? `"${conteudo[f]}"` : conteudo[f];
                    let result = eval(`(() => {${rule.replace(f,fiedlValue)}})()`);
                    curField += result || 0;
                }

                curEntity[f] = curField;

            }
        }

        return curEntity;
    }

    this.parseCases = function(dados){
        let lstCases = "";

        for(let record of dados.records){
            ({end, start} = record._fields[0]);

            const customer = start.properties;
            const payment = end.properties;

            customer["entity"] = "Customer";
            payment["entity"] = "Payment";

            let customerScores = cases.viewController.scoreByEntity(customer);
            let paymentScores = cases.viewController.scoreByEntity(payment);
            let processScore = customerScores.total + paymentScores.total;

            const process = {
                city: customer.city,
                amount: payment.amount,
                date: timeUtil.aoDate(payment.paymentDate.toString()),
                id: payment.code,
                idCustomer: customer.code,
                score: processScore 
            }
    
            lstCases += cases.viewController.generateCaseRow(process);

        }

        document.getElementById("allcases").innerHTML = lstCases;
        //cases.viewController.setupCaseDblClickEvent();
    }

    this.generateCaseRow = function(obj){
        
        caseSeverity = 'case-low';

        if(obj.score > 40 && obj.score < 50){
            caseSeverity = 'case-med';
        }
        
        if(obj.score > 49){
            caseSeverity = 'case-high';
        }
        
        let row = `
                    <tr class="caseRow" data-process-id="${obj.id}">
                        <td class="text-center text-muted">
                            <div class="case-id ${caseSeverity}"> #${obj.score} </div>
                        </td>
                        <td class="text-center text-muted">${obj.id}</td>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <!--
                                    <div class="widget-content-left mr-3">
                                        <div class="widget-content-left">
                                            <img width="40" class="rounded-circle" src="assets/images/avatars/4.jpg" alt="">
                                        </div>
                                    </div>
                                    -->
                                    <div class="widget-content-left flex2">
                                        <div class="widget-heading">Alto risco</div>
                                        <div class="widget-subheading opacity-7">Declaração de importação inconsistente</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">${obj.city}</td>
                        <td class="text-center">${obj.date}</td>
                    </tr>
            `;
        
        return row;

    }

    return this;
}