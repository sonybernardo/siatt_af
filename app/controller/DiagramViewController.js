diagram = {
    viewController : new DiagramViewController(),
    selectedNode : null,
    selectedNodeType : null,
    lstNodes: [],
    nodesRel: [],
    addedNode: [],
    addedRel: [],
}


function DiagramViewController(){

    this.init = function(){

        queryString = 'MATCH (n:Customer) RETURN *';
        __neo4j__.connection.fetchData(queryString, diagram.viewController.renderReturnedData);

    }

    this.renderObjectLinks = function(){

        let queryString = `MATCH (o:${diagram.selectedNodeType}), (o)-[l]-(r) WHERE ID(o) = ${diagram.selectedNode} RETURN *`;
        console.log(queryString);
        __neo4j__.connection.fetchData(queryString, diagram.viewController.renderReturnedData);
        this.hideMenuContexto();

    }

    this.filterCase = function(nodeCode){

        let queryString = `MATCH (e1:Payment), (e1)-[l]-(r) WHERE e1.code = "${nodeCode}" RETURN *`;
        console.log(queryString);
        __neo4j__.connection.fetchData(queryString, diagram.viewController.renderReturnedData);
        this.hideMenuContexto();

    }

    this.filterCaseForNetwork = function(e){

        let tecla = event.keyCode;

        if(tecla == 13){

            let text = document.getElementById("filteringText").value;
    
            let queryString = `MATCH (c:Customer) WHERE c.customerName =~ ".*(?i)${text}.*" RETURN *`;
            console.log(queryString);
            __neo4j__.connection.fetchData(queryString, diagram.viewController.renderReturnedData);
            this.hideMenuContexto();
        
        }

    }

    this.resetDiagram = function(){

        diagram.addedNode = [];
        diagram.lstNodes = [];
        diagram.nodesRel = [];

    }

    this.renderReturnedData = function(result){
        
        diagram.viewController.resetDiagram();

        for(n of result.records){

            n._fields.forEach(f => {
                
                if(f.hasOwnProperty("start") && f.hasOwnProperty("end")){
                
                    let from = f.start.low;
                    let to = f.end.low;
                    diagram.nodesRel.push({from: from, to: to, label: f.type, arrows: "to" });

                }else{

                    if(!diagram.addedNode.includes(f.identity.low)){
                        
                        diagram.lstNodes.push({
                            id: f.identity.low,
                            label: f.properties.label,
                            group: f.labels[0]
                        });
                        diagram.addedNode.push(f.identity.low);
                    }
                }
            });
        }
        diagram.viewController.renderDiagram();
    }
    

    this.genereteNodes = function(){
        return diagram.lstNodes;
    }

    this.generateLinks = function(){
        return diagram.nodesRel;
    }

    this.filterNodeById = function(id){
        
        let nodes = this.genereteNodes();
        return nodes.filter(n => n.id == id)[0];

    }

    this.getNodes = function(){

        let nos = new vis.DataSet(this.genereteNodes());
        let arestas = new vis.DataSet(this.generateLinks());

        let dados = {nodes:nos,edges: arestas}

        return dados;
    }

    this.getOpcoes = function(){

        let opcoes = {
            groups: nodesIco,
        }

        return opcoes;

    }

    this.renderDiagram = function(){

        var container = document.getElementById("localDiagrama");

        var grafo = new vis.Network(container,diagram.viewController.getNodes(),diagram.viewController.getOpcoes());

        grafo.on("click", function(props){
            
            diagram.selectedNode = null;
            document.getElementById("menuContexto").style.display = "none";
            console.log("Click normal ",props);

            if(props.nodes.length > 0){
                diagram.selectedNode = props.nodes[0];
                let curNode = diagram.viewController.filterNodeById(diagram.selectedNode);
                diagram.selectedNodeType = curNode.group
            }

            
        });

        grafo.on("oncontext", function (params) {

            params.event.preventDefault();
            
            if(diagram.selectedNode != null){

                let curNode = diagram.viewController.filterNodeById(diagram.selectedNode);
                console.log(curNode);
                document.getElementById("selectedNodeName").innerHTML = curNode.label;

                let elm = document.getElementById("menuContexto");
                elm.style.display = "block";
                elm.style.top = params.event.pageY + "px";
                elm.style.left = params.event.pageX + "px";

            }

        });

    }

    this.hideMenuContexto = function(){
        elm = document.getElementById("menuContexto").style.display = "none";
    }

    return this;

}