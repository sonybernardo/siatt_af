pannel = {
    viewController: new DashboardViewController(),
}

listStatus = {"show": "block", "hide": "none"}

function DashboardViewController(){

    this.configuration = function(){

        let configView = document.querySelector(".ui-theme-settings");
        
        if(!configView.classList.contains("settings-open"))
            document.getElementById("TooltipDemo").click();
    }

    this.hideConfiguration = function(){
        document.querySelector(".ui-theme-settings").classList.remove("settings-open");
        view.activeConfigView = null;
    }

    this.init = function(){

        pannel.viewController.getCases();
        pannel.viewController.getTotalCases();
        pannel.viewController.getTotalImportacoes();
        pannel.viewController.getTotalImportadores();
        _case.viewController.dataTableContextMenu();
            
    }

    this.getCases = function(){

        let queryString = `MATCH p=(c:Customer)-[r]->(py:Payment) WHERE NOT EXISTS(py.submetido) RETURN p LIMIT 4`;
        __neo4j__.connection.fetchData(queryString, pannel.viewController.parseCases);

    }

    this.getTotalCases = function(){

        let queryString = `MATCH p=(:Customer)-[r]->(py:Payment) RETURN count(py) as total`;
        __neo4j__.connection.fetchData(queryString, function(result){

            document.getElementById("totalCases").innerHTML = result.records[0]._fields[0].low;

        });

    }

    this.getTotalImportadores = function(){

        let queryString = `MATCH p=(:Customer) RETURN count(p) as total`;
        __neo4j__.connection.fetchData(queryString, function(result){

            document.getElementById("totalImportadores").innerHTML = result.records[0]._fields[0].low;

        });

    }

    this.getTotalImportacoes = function(){

        let queryString = `MATCH p=(:Customer)-[r]->(o:Order) RETURN count(o) as total`;
        __neo4j__.connection.fetchData(queryString, function(result){

            document.getElementById("totalImportacoes").innerHTML = result.records[0]._fields[0].low;

        });

    }
    
    this.scoreByEntity = function(entity){

        let allScrores = pannel.viewController.calcScore(entity);
        let totalScore = 0;

        for(let i in allScrores){
            totalScore += allScrores[i];
        }

        return {
            total: totalScore,
            byfield: allScrores,
        };

    }

    this.parseCases = function(dados){

        let lstCases = "";

        for(let record of dados.records){

            ({end, start} = record._fields[0]);
            
            const customer = start.properties;
            const payment = end.properties;

            customer["entity"] = "Customer";
            payment["entity"] = "Payment";
            
            let customerScores = pannel.viewController.scoreByEntity(customer);
            let paymentScores = pannel.viewController.scoreByEntity(payment);
            let processScore = customerScores.total + paymentScores.total;

            console.log(customerScores);

            const process = {
                city: customer.city,
                amount: payment.amount,
                date: timeUtil.aoDate(payment.paymentDate.toString()),
                id: payment.code,
                idCustomer: customer.code,
                score: processScore,
                country : customer.country
            }
    
            lstCases += pannel.viewController.generateCaseRow(process);
            
        }

        document.getElementById("reportedCases").innerHTML = lstCases;
        _case.viewController.setupCaseDblClickEvent();

    }

    this.calcScore = function(conteudo){

        if(!rule.allRule.hasOwnProperty(conteudo.entity))
            return [];

        let rulesToApply = rule.allRule[conteudo.entity];
        let fieldsToApply = Object.keys(conteudo);

        let curEntity = {};

        for(let f of fieldsToApply){
            
            if(rulesToApply.hasOwnProperty(f)){

                let curRules = rulesToApply[f];
                let curField = 0;

                for(let rule of curRules){
                
                    let fieldValue = isNaN(conteudo[f]) ? `"${conteudo[f]}"` : conteudo[f];
                    let result = eval(`(() => {${rule.replace(f,fieldValue)}})()`);
                    curField += result || 0;

                }

                curEntity[f] = curField;
            }
        }

        return curEntity;
        

    }


    this.analiseCase = function(){
        view.navigation.callViewWithHandler('new-case',() => {});
    }

    this.filterCase = function(){

    }

    this.generateCaseRow = function(obj){
        
        caseSeverity = 'case-low';

        if(obj.score > 40 && obj.score < 50){
            caseSeverity = 'case-med';
        }
        
        if(obj.score > 49){
            caseSeverity = 'case-high';
        }
        
        let row = `
                    <tr class="caseRow" data-process-id="${obj.id}">
                        <td class="text-center text-muted">
                            <div class="case-id ${caseSeverity}"> #${obj.score} </div>
                        </td>
                        <td class="text-center text-muted">${obj.id}</td>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <!--
                                    <div class="widget-content-left mr-3">
                                        <div class="widget-content-left">
                                            <img width="40" class="rounded-circle" src="assets/images/avatars/4.jpg" alt="">
                                        </div>
                                    </div>
                                    -->
                                    <div class="widget-content-left flex2">
                                        <div class="widget-heading">Alto risco</div>
                                        <div class="widget-subheading opacity-7">Declaração de importação inconsistente</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">${obj.country}</td>
                        <td class="text-center">${obj.city}</td>
                        <td class="text-center">${obj.date}</td>
                    </tr>
            `;
        
        return row;

    }

    
    this.fetchFromOrigin = function(){
        
        
        let request = new RequestObject();
        
        
        let requestParams = {
            url : "http://localhost:3000/sync",
            showModal: false,
            callBack : (r) => {
                console.log(r);
                this.handleLastUpdate('show');
                pannel.viewController.handleSpinner('hide');
                timeUtil.stopCountSeconds(updateTimeCounter);
            }
        }
        
       this.handleLastUpdate('hide');
       this.handleSpinner('show');
       
       request.get(requestParams);
       updateTimeCounter = timeUtil.countSeconds("updatingTimer");
       
    }
    
    this.handleLastUpdate = function(stat){
        document.getElementById("lastUpdateContainer").style.display = listStatus[stat];
        document.getElementById("updatingTimer").style.display = (stat == "show" ? "none" : "block");
    }

    this.handleSpinner = function(stat){
        document.querySelector(".bolSpinnerLogin").style.display = listStatus[stat];
    }

    return this;

}