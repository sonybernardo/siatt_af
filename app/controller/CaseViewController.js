_case = {
    viewController: new CaseViewController(),
    selsectedId : null,
}

function CaseViewController(){

    this.init = function(){
        _case.viewController.getCases();
        _case.viewController.dataTableContextMenu();

    }

    this.getCases = function(){

        let queryString = `MATCH p=(:Customer)-[r]->(:Payment) RETURN p`;
        __neo4j__.connection.fetchData(queryString, _case.viewController.parseCases );

    }

    this.scoreByEntity = function(entity){

        let allScores = _case.viewController.calcScore(entity);
        let totalScore = 0;

        for(let i in allScores){
            totalScore += allScores[i];
        }

        return {
            total: totalScore,
            byfield: allScores,
        };
    }

    this.calcScore = function(conteudo){
        if(!rule.allRule.hasOwnProperty(conteudo.entity))
            return [];
        
        let ruleToApply = rule.allRule[conteudo.entity];
        let filedsToApply = Object.keys(conteudo);

        let curEntity = {};

        for(let f of filedsToApply){
            if(ruleToApply.hasOwnProperty(f)){

                let curRule = ruleToApply[f];
                let curField = 0;

                for(let rule of curRule){

                    let fiedlValue = isNaN(conteudo[f]) ? `"${conteudo[f]}"` : conteudo[f];
                    let result = eval(`(() => {${rule.replace(f,fiedlValue)}})()`);
                    curField += result || 0;
                }

                curEntity[f] = curField;

            }
        }

        return curEntity;
    }

    this.parseCases = function(dados){
        let lstCases = "";

        for(let record of dados.records){
            ({end, start} = record._fields[0]);

            const customer = start.properties;
            const payment = end.properties;

            customer["entity"] = "Customer";
            payment["entity"] = "Payment";

            let customerScores = _case.viewController.scoreByEntity(customer);
            let paymentScores = _case.viewController.scoreByEntity(payment);
            let processScore = customerScores.total + paymentScores.total;

            const process = {
                city: customer.city,
                amount: payment.amount,
                date: timeUtil.aoDate(payment.paymentDate.toString()),
                id: payment.code,
                idCustomer: customer.code,
                score: processScore,
                country: customer.country,
                submetido: payment.submetido
            }
    
            lstCases += _case.viewController.generateCaseRow(process);
            
        }

        document.getElementById("reportedCases").innerHTML = lstCases;
        _case.viewController.setupCaseDblClickEvent();
        //cases.viewController.setupCaseDblClickEvent();
    }

    this.generateCaseRow = function(obj){
        
        caseSeverity = 'case-low';

        if(obj.score > 40 && obj.score < 50){
            caseSeverity = 'case-med';
        }
        
        if(obj.score > 49){
            caseSeverity = 'case-high';
        }
        
        let status = `<span class="em-fila">Em fila</span>`;
        let statusIdentify = "em-fila";
        if(obj.submetido == "sim"){
            status = `<span class="processado">Submetido para analise</span>`;
            statusIdentify = "processado"
        }

        let row = `
                    <tr class="caseRow _${caseSeverity} _${statusIdentify}" data-process-id="${obj.id}">
                        <td class="text-center text-muted">
                            <div class="case-id ${caseSeverity}"> #${obj.score} </div>
                        </td>
                        <td class="text-center text-muted">${obj.id}</td>
                        <td class="text-center text-muted">${status}</td>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <!--
                                    <div class="widget-content-left mr-3">
                                        <div class="widget-content-left">
                                            <img width="40" class="rounded-circle" src="assets/images/avatars/4.jpg" alt="">
                                        </div>
                                    </div>
                                    -->
                                    <div class="widget-content-left flex2">
                                        <div class="widget-heading">Alto risco</div>
                                        <div class="widget-subheading opacity-7">Declaração de importação inconsistente</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">${obj.country}</td>
                        <td class="text-center">${obj.city}</td>
                        <td class="text-center">${obj.date}</td>
                    </tr>
            `;
        
        return row;

    }

    this.dataTableContextMenu = function(){

        let table = document.getElementById("reportedCases");
        //let scrollPos = document.documentElement.scrollTop;

        if(table){
            table.addEventListener('contextmenu', (e) => {
                
                e.preventDefault();
                _case.selsectedId = _case.viewController.menuContextSelectedLine(e);

                let menu = document.getElementById("menuContextoCasoDatatable");
                let scrollPos = document.documentElement.scrollTop

                menu.style.position = "absolute";
                menu.style.left = e.clientX+"px";
                menu.style.top = (e.clientY + scrollPos)+"px";
                menu.style.display = "block";
                menu.style.zIndex = "100";

                document.addEventListener("scroll", () => {
                    _case.viewController.hideDataTableContextMenu();
                });

            });

            document.onclick = function(){
                _case.viewController.hideDataTableContextMenu();
            }

        }

    }

    this.menuContextSelectedLine = function(e){
        
        clickedNode = e.target.parentNode;
        
        for(x = 0; x < 16; x++){

            if(clickedNode.nodeName.toString().toLowerCase() != "tr"){
                clickedNode = clickedNode["parentNode"];
            }else{
                console.log("Encontrou na posição: ",x);
                break;
            }
        }
        return clickedNode.getElementsByTagName('td')[1].innerHTML;
    }

    this.hideDataTableContextMenu = function(){

        let menu = document.getElementById("menuContextoCasoDatatable");
        menu.style.display = "none";

    }

    this.submiteCase = function(){

        let idCase = _case.selsectedId;

        try{

            let queryString = `MATCH (p:Payment {code:"${idCase}"}) SET p.submetido = "sim" RETURN p`;
            __neo4j__.connection.writeData(queryString, _case.viewController.parseSubmitedCase);
        
        }catch(e){
            console.log(e);
        }
        
    }

    this.parseSubmitedCase = function(result){

        //console.log("Dados submetidos: ",result);
        if( view.activeView == "cases")
            _case.viewController.getCases();
        else
            pannel.viewController.getCases();

    }


    this.setupCaseDblClickEvent = function(){

        let cases = document.querySelectorAll(".caseRow");
        for(let curCase of cases){
            curCase.ondblclick = function(){

                view.navigation.callViewWithHandler('new-case', function(){
                    
                    diagram.viewController.filterCase(curCase.dataset.processId);

                });
            }
        }

    }

    this.filterByStatus = function(status){

        let allCases = document.querySelectorAll(".caseRow");

        for(let i = 0; i < allCases.length; i++){

            if(allCases[i].classList.contains(`_${status}`)){
                allCases[i].style.display ="";
            } else {
                allCases[i].style.display = "none";
            }
        }
    }


    this.filterBySeverity = function(level){

        let allCases = document.querySelectorAll(".caseRow");

        for(let x = 0; x < allCases.length; x++){

            console.log(allCases[x].classList.contains(level));

            if(allCases[x].classList.contains(`_${level}`)){
                allCases[x].style.display = "";
            }else{
                allCases[x].style.display = "none";
            }

        }
        //document.getElementById("reportedCases").innerHTML = filteredCases;

    }


    return this;

}