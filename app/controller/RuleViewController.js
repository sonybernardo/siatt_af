rule = {
    viewController : new RuleViewController(),
    addedFieldRule: [],
    allRule: {},
}


function RuleViewController(){

    this.addedFieldRule = [];

    this.getAllRules = function(){

        let request = new RequestObject();
        request.get({
            url: "http://localhost:3001/regras",
            callBack: rule.viewController.parseAllRules,
            showModal: false,
        })
    
    }

    this.parseAllRules = function(result){

        let dados = JSON.parse(result);
        rule.allRule = {};

        for(r of dados){
            //console.log(r.entidade," ",r.conteudo);
            if(!rule.allRule.hasOwnProperty(r.entidade)){

                rule.allRule[r.entidade] = {};
            }

            ruleConteudo = rule.viewController.extractRule(r.conteudo);
            
            if(!rule.allRule[r.entidade].hasOwnProperty(ruleConteudo.field)){
                rule.allRule[r.entidade][ruleConteudo.field] = [];
            }

            rule.allRule[r.entidade][ruleConteudo.field].push(r.conteudo);

        }

        //if(!rule.allRule.hasOwnProperty())

    } 



    this.init = function(){

        queryString = 'MATCH (n) RETURN distinct labels(n)';
        __neo4j__.connection.fetchData(queryString, (res) => {

            let rows = "";
            for(record of res.records){
                rows += rule.viewController.generateRow(record._fields[0]);
            }
            document.getElementById("entityTablesBody").innerHTML = rows;

        });
    
    }


    this.getProperties = function(entity){

        queryString = `MATCH (n:${entity}) RETURN DISTINCT properties(n) LIMIT 1`;
        __neo4j__.connection.fetchData(queryString, (res) => {
            
            let fields = Object.keys(res.records[0]._fields[0]);
            let listOfRules = "";

            for(f of fields){
                listOfRules += rule.viewController.generateFiledGroup(entity,f);
            }
            document.getElementById("listOfRules").innerHTML = listOfRules;
            document.getElementById("rulesContent").style.visibility = "visible";

            rule.viewController.findRegras(entity);

        });
    
    }

    this.callView = function(){
        
        rule.viewController.init();
        pannel.viewController.configuration();

    }


    this.generateRow = function(obj){

        let row = `
                    <tr>
                        <td align="right">
                            <input type="radio" name="entity" value="${obj}" 
                                   onclick="rule.viewController.getProperties('${obj}')" />
                        </td>
                        <td>${obj}</td>
                    </tr>
        `;
        return row;

    }

    this.generateRuleRow = function(obj){

        let row = `
                    <tr>
                        <td align="right">
                            <input type="text" value="${obj}" 
                                   onclick="rule.viewController.getProperties('${obj}')" />
                        </td>
                        <td>${obj}</td>
                    </tr>
        `;
        return row;

    }

    this.generateFiledGroup = function(entity,fieldName){

        return `
            
            <div id="${fieldName}" class="fieldRuleGrupo">
                <span class="fieldSetTitle"><b>${fieldName}</b></span>

                <div>

                    <table class="rulesTable">

                        <thead>
                            <tr>
                                <td>Comparador</td>
                                <td>Valor</td>
                                <td>Score ou Prop.</td>
                            </tr>
                        <thead/>

                        <tbody id="${entity}.${fieldName}">

                            <tr class="groupRule">
                            
                                <td>
                                    <select class="comparadorRule">
                                        <option value="">Selecione</option>
                                        <option value="==">Igual à</option>
                                        <option value=">">Maior que</option>
                                        <option value="<">Menor que</option>
                                        <option value="!=">Diferente de</option>
                                    </select>
                                </td>

                                <td>
                                    <input 
                                        type="text" 
                                        class="ruleFieldContent" 
                                        data-entity="${entity}" 
                                        data-field="${fieldName}" 
                                        placeholder="Digite o valor">
                                </td>

                                <td>
                                    <input 
                                        type="text"
                                        class="ruleFieldValue"
                                        placeholder="Score">
                                    
                                </td>

                            </tr>
                        
                        </tbody>

                    </table>

                </div>

                <span 
                    class="btnAddRule"
                    onclick="rule.viewController.newRuleLine('${entity}','${fieldName}')"
                    >+</span>
            </div>
        `;

    }


    this.getRulesValue = function(){

        loadedFields = {}

        let allFields = document.querySelectorAll(".ruleFieldContent");
        let comparador = document.querySelectorAll(".comparadorRule");
        let scoreValues = document.querySelectorAll(".ruleFieldValue");

        x = 0;
        for(f of allFields){
            
            if(!loadedFields.hasOwnProperty(f.dataset.entity)){
                loadedFields[f.dataset.entity] = {};
            }

            if(!loadedFields[f.dataset.entity].hasOwnProperty(f.dataset.field)){
                loadedFields[f.dataset.entity][f.dataset.field] = [];
            }

            if(f.value != "" && scoreValues[x].value != ""){

                let finalRule = rule.viewController.scoreBusinessRule({
                    field: f.dataset.field,
                    compare: comparador[x].value,
                    value: f.value,
                    score: scoreValues[x].value
                });
                //comparador[x].value+" "+f.value+" entao "+scoreValues[x].value;
                loadedFields[f.dataset.entity][f.dataset.field].push(finalRule);

            }
            x++;
        }

    }

    this.scoreBusinessRule = function({field,compare,value,score}){

        if(isNaN(value)) value = `"${value}"`;

        return `
            if(${field} ${compare} ${value}){ return ${score}; }
        `.replace(/\n/g,"").replace(/\r/g,"");

    }
    
    this.saveRules = function(){

        rule.viewController.getRulesValue()
        let request = new RequestObject();

        let params = {
            url:"http://localhost:3001/regras",
            form: JSON.stringify(loadedFields),
            json: true,
            callBack : () => {

                rule.viewController.getAllRules();
                
                if(view.activeView == "dashaboard"){
                    console.log("Updaing...");
                    pannel.viewController.getCases();
                }

                if(view.activeView == "cases")
                    _case.viewController.getCases();

            }
        }

        request.post(params);

    }

    this.findRegras = function(entity){
        rule.viewController.parseLoadedRegras(entity);
        /*
        let request = new RequestObject();
        request.get({
            url: "http://localhost:3001/regras/"+entity,
            callBack: rule.viewController.parseRegrasFromDb
        })
        */

    }


    this.parseLoadedRegras = function(entity){

        let lstRules = rule.allRule[entity];
        rule.addedFieldRule = [];

        let index = 0;

        for(let i in lstRules){

            for(let curRule of lstRules[i]){

                let obj = {entidade: entity, conteudo: curRule, id: index};
                rule.viewController.generateRegraFromDb(obj);
                index++;
            }

        }
    }

    this.parseRegrasFromDb = function(res){

        let dados = JSON.parse(res);

        for(i in dados){
            let listRules = rule.viewController.generateRegraFromDb(dados[i]);
        }

    }


    this.ruleCompareComponent = function(tokenVal){
        
        let listOpcoes = {
            "": "Selecione",
            "==": "Igual à",
            ">": "Maior que",
            "<": "Menor que",
            "!=": "Diferente de",
        };

        let elm = document.createElement("select");
        elm.className = "comparadorRule";
        
        for(token in listOpcoes){
            let option = document.createElement("option");
            option.value = token;
            if(tokenVal == token)
                option.selected = true;
            option.innerHTML = listOpcoes[token];
            elm.appendChild(option);
        }

        return elm;
        
    }

    this.ruleValueComponent = function({entidade,field,valor}){

        let elm = document.createElement("input");
        elm.dataset.entity = entidade;
        elm.dataset.field = field;
        
        elm.value = valor;
        elm.type = "text";
        elm.className = "ruleFieldContent";
        elm.placeholder = "Digite o valor";

        return elm;

    }

    this.ruleScoreComponent = function(valor){

        let elm = document.createElement("input");
        
        elm.value = valor;
        elm.type = "text";
        elm.className = "ruleFieldValue";
        elm.placeholder = "Scrore";

        return elm;

    }

    this.newRuleLine = function(entity, fieldName){

        let comparador = rule.viewController.ruleCompareComponent('');
        let valor = rule.viewController.ruleValueComponent({entidade: entity, field: fieldName, valor: ""});
        let score = rule.viewController.ruleScoreComponent('');

        let row = document.createElement("tr");
        let colCompare = document.createElement("td");
        let colValue = document.createElement("td");
        let colScore = document.createElement("td");

        colCompare.appendChild(comparador);
        colValue.appendChild(valor);
        colScore.appendChild(score);

        row.appendChild(colCompare);
        row.appendChild(colValue);
        row.appendChild(colScore);

        document.getElementById(`${entity}.${fieldName}`).appendChild(row);

    }

    this.removeBdRule = function({entity, field, id}){

        let line = document.getElementById(`bdRule${id}`);
        document.getElementById(`${entity}.${field}`).removeChild(line);

    }
    
    this.generateRegraFromDb = function({id, conteudo, entidade}){

        let ruleValues = rule.viewController.extractRule(conteudo);
        let ruleObj = {...ruleValues,entidade: entidade}

        let row = document.createElement("tr");
        let colCompare = document.createElement("td");
        let colValue = document.createElement("td");
        let colScore = document.createElement("td");

        let compare = rule.viewController.ruleCompareComponent(ruleObj.token);
        let value = rule.viewController.ruleValueComponent({...ruleObj, entidade: entidade});
        let score = rule.viewController.ruleScoreComponent(ruleObj.score);

        row.id = `bdRule${id}`;

        let removeBtn = document.createElement("span");
        removeBtn.className = "btnAddRule removeButton";
        removeBtn.innerHTML = `<i class="fa fa-times"></i>`;
        removeBtn.onclick = function(){
            rule.viewController.removeBdRule({entity: entidade, field: ruleObj.field, id: id});
        }

        colScore.style.display = "flex";

        colCompare.appendChild(compare);
        colValue.appendChild(value);
        colScore.appendChild(score);
        colScore.appendChild(removeBtn);

        row.appendChild(colCompare);
        row.appendChild(colValue);
        row.appendChild(colScore);

        if(!rule.addedFieldRule.includes(`${entidade}.${ruleObj.field}`)){

            document.getElementById(`${entidade}.${ruleObj.field}`).innerHTML = "";
            rule.addedFieldRule.push(`${entidade}.${ruleObj.field}`);

        }

        document.getElementById(`${entidade}.${ruleObj.field}`).appendChild(row);

    }

    this.extractRule = function(conteudo){

        
        let conteudoArr = conteudo.split(" ");
        let field = conteudoArr[0].replace("if(","");
        let token = conteudoArr[1];
        let valor = conteudoArr[2]
                    .replace(/\"/g,"")
                    .replace("){").replace("undefined","");
        let score = conteudoArr[4].replace(";","");

        return {
            field: field,
            token: token,
            valor: valor,
            score: score
        } 
        

    }

    return this;

}

