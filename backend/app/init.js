const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");
const Regra = require("../models/Regra");


app.use(cors());
app.use(bodyParser.json())

app.post("/regras", (req, res) => {

    let currentTime = Date.now();
    let entity = Object.keys(req.body);
    let fields = Object.keys(req.body[entity]);

    for(f of fields){
        
        for(regra of req.body[entity][f]){
            Regra.save({entidade: entity, conteudo:`${regra}`,time: currentTime});
        }
        Regra.deleteOld({entidade: entity, time:currentTime})

    }
    
    res.send("Ola pessoal");
});

app.get("/regras", (req,res) => {

    Regra.findAllRegras(function(err, result){
        
        if(err)
            return console.log("Erro ao buscar todas as regras: ",err);
        res.send(result);

    });

});

app.get("/regras/:entity", (req,res) => {

    Regra.findRegras(req.params.entity,function(err, result){
        
        if(err)
            return console.log("Erro ao buscar os dados: ",err);

        console.log(result);
        res.send(result);

    })
    

});

app.listen(3001, () => console.log("Servidor corrento"));


