const mysql = require("mysql");


const cx = mysql.createConnection({
    user: "root",
    password: "",
    database: "vi"
});

function save({entidade, conteudo, time}){

    let queryString = `
                        INSERT INTO regra SET 
                        entidade = '${entidade}',
                        conteudo = '${conteudo.toString().trim()}',
                        time = '${time}'
                        -- (entidade, conteudo) VALUES ('${entidade}','${conteudo}')
                      `;
    
    cx.query(queryString, (err,result) => {
        
        if(err)
            return console.log("Erro ao executar BD: ",err);
        console.log(result);
        
    });
    
}

function deleteOld({entidade, time}){
    cx.query(`DELETE FROM regra WHERE entidade = '${entidade}' AND time <> ${time}`);
}

function findRegras(entidade, callBack){

    cx.query(`SELECT * FROM regra WHERE entidade = '${entidade}'`,callBack)

}

function findAllRegras(callBack){

    cx.query(`SELECT * FROM regra`,callBack)

}


module.exports = {
    save : save,
    deleteOld: deleteOld,
    findRegras: findRegras,
    findAllRegras : findAllRegras,
}